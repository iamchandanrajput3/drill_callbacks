const getBoardsInfo = require('./callback1.js')
const getLists = require('./callback2.js');
const getCards = require('./callback3.js');

function getCardForMindAndSpaceList(id) {
    if(typeof id != 'string' || id === '')
    {
        throw new Error('Invalid Argument');
    }

    getBoardsInfo(id, function(response){
        console.log(response);
        getLists(response[0].id, function(nextResponse){
            console.log(nextResponse);
            let listId = [];
            for(let i=0; i<nextResponse.length;i++)
            {
                if(nextResponse[i].name === 'Mind' || nextResponse[i].name === 'Space')
                {
                    listId.push(nextResponse[i].id);
                }
            }
            listId.forEach((ele) =>{
                getCards(ele,function(finalResponse){
                    console.log(finalResponse);
                });
            });
        });
    });
}

module.exports = getCardForMindAndSpaceList;