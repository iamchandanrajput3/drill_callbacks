const boards = require('./data/boards.json');

function getBoardsInfo(boardId,cb) {
    if(typeof boardId !== 'string' || typeof boardId === 'undefined' || typeof cb !== 'function' || boardId === '')
    {
        throw new Error('Invalid arguement passed');
    }

    let data = boards.filter((obj)=> obj.id === boardId )
    setTimeout(() =>{cb(data)},2000); 
}

module.exports = getBoardsInfo;