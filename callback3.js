const cards = require('./data/cards.json');

function getCards(listId,cb) {
    if(typeof listId !== 'string' || typeof listId === 'undefined' || listId === '' || typeof cb !== 'function')
    {
        throw new Error('Invalid Arguments Passed');
    }

    let data = Object.entries(cards).filter((element) =>element[0] === listId); 
    if(data.length !== 0)
    {
      var cardsBasedOnListId = data[0][1];
    }

    setTimeout(() =>{cb(cardsBasedOnListId);}, 2000);
}

module.exports = getCards;