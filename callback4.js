const getBoardsInfo = require('./callback1.js')
const getLists = require('./callback2.js');
const getCards = require('./callback3.js');


function getCardForMindList(id) {
    if(typeof id != 'string' || id === '')
    {
        throw new Error('Invalid Argument');
    }

    getBoardsInfo(id, function(response){
        console.log(response);
        getLists(response[0].id, function(nextResponse){
            console.log(nextResponse);
            let list = nextResponse.find((ele)=>ele.name === 'Mind');
            let listId = list.id;
            getCards(listId, function(finalResponse){
                console.log(finalResponse);
            });
        });
    });
}

module.exports = getCardForMindList;