const lists = require('./data/lists.json');

function getLists(boardId,cb){
    if(typeof boardId !== 'string' || typeof boardId === 'undefined' || boardId === '' || typeof cb !== 'function')
    {
        throw new Error('Invalid arguments passed');
    }
    
    let data = Object.entries(lists).filter((element) =>element[0] === boardId);
    let listBasedOnId = data[0][1];
    
    setTimeout(()=>{cb(listBasedOnId);},2000);
}

module.exports = getLists;